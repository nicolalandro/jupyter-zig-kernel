const std = @import("std");

pub fn main() !void {
    std.debug.print("Two.\n", .{});
    var one = try std.DynLib.open("./libone.so");
    const sample_print_one = one.lookup(*const fn () void, "sample_print") orelse return error.SymbolNotFound;
    sample_print_one();
}