pub export fn sample_print() void {
    const std = @import("std");
    std.debug.print("One.\n", .{});
}

pub fn main() !void {
    sample_print();
}