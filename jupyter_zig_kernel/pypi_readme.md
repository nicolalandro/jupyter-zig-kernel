# Zig Jupyter Kernel

This is a simple jupyter kernel for [zig](https://ziglang.org/) language.

## How to install
You can install with following commands
```
pip install jupyter_zig_kernel
install_zig_kernel

# run jupyter and enjoy zig kernel
jupyter notebook
```