from ipykernel.kernelbase import Kernel
from subprocess import check_output, PIPE, Popen
import os
import tempfile

class ZigKernel(Kernel):
    # standard settings
    implementation = 'Zig'
    implementation_version = '1.0'
    language = 'zig'
    
    _lang_version = None
    @property
    def language_version(self):
        if self._lang_version is None:
            self._lang_version = check_output(['zig', 'version']).decode('utf-8')
        return self._lang_version

    @property
    def banner(self):
        return f"Zig {self._lang_version}"

    language_info = {'name': 'zig',
                     'mimetype': 'text/plain',
                     'file_extension': '.zig'}
   
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.files = []

    def cleanup_files(self):
        """Remove all the temporary files created by the kernel"""
        for file in self.files:
            os.remove(file)

    def new_temp_file(self, **kwargs):
        """Create a new temp file to be deleted when the kernel shuts down"""
        # We don't want the file to be deleted when closed, but only when the kernel stops
        kwargs['delete'] = False
        kwargs['mode'] = 'w'
        file = tempfile.NamedTemporaryFile(**kwargs)
        self.files.append(file.name)
        return file, file.name
    
    def _filter_magics(self, code):
        magic = None
        if code.startswith('//%'):
            magic = code.splitlines()[0][3:].strip().split(' ')
        return magic
        
    def do_execute(self, code, silent, store_history=True, user_expressions=None,
                   allow_stdin=False):
        if not silent:
            magic = self._filter_magics(code)

            tmp_file, tmp_file_name = self.new_temp_file(suffix='.zig')
            with tmp_file as source_file:
                source_file.write(code)
                source_file.flush()

            if magic:
                pipes = Popen(['zig'] + magic + [tmp_file_name], stdout=PIPE, stderr=PIPE)
            else:
                pipes = Popen(['zig', 'run', tmp_file_name], stdout=PIPE, stderr=PIPE)
            std_out, std_err = pipes.communicate()

            self.send_response(self.iopub_socket, 'stream', {'name': 'stderr', 'text': std_err.decode('utf-8')})
            self.send_response(self.iopub_socket, 'stream', {'name': 'stdout', 'text': std_out.decode('utf-8')})

        return {'status': 'ok',
                # The base class increments the execution count
                'execution_count': self.execution_count,
                'payload': [],
                'user_expressions': {},
               }

    def do_shutdown(self, restart):
        """Cleanup the created source code files and executables when shutting down the kernel"""
        self.cleanup_files()
