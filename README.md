# Zig Kernel for Jupyter
This repo contain a first attempt to create a zig kernel for jupyter.

![example screnshot](imgs/example.png)

## How to install

```
# prerequirements: python3, pip, jupyter notebook

# install jupyter zig kernel
pip install jupyter-zig-kernel --index-url https://gitlab.com/api/v4/projects/47990176/packages/pypi/simple
install_zig_kernel # on Mac M1/M2 maybe that command can have some problems, you can run it by searching into builded app

# run jupyter
jupyter notebook
```


# Important TODO list

* [x] Kernel exec separatelly each cell
* [x] Docekrfile
* [x] Kernel magic command as compiler params (to exec also test and not only run)
* [ ] Automation test
* [ ] Possibility to add library
* [ ] Run cell like in the same file

## How to dev
* build with docker
```
docker compose build
```
* run a terminal into container
```
docker compose run jupyter ash
```
* run jupyter
```
docker compose up
# localhost:8888
```

## [WIP] Dyn example
This is tentative to made a [REPL (real-eval-print loop)](https://en.wikipedia.org/wiki/Read%E2%80%93eval%E2%80%93print_loop) with zig that will allow to have the experience like a real notebook.

```
docker-compose run jupyter ash
cd notebooks/zig_dyn_example
rm -rf libone.*
zig build-lib -lc -dynamic -isystem . one.zig
zig run two.zig
```

The same thing is made for c++ in the following projects:
* [igcc](https://github.com/alexandru-dinu/igcc)
* [cling](https://github.com/root-project/cling)

# References
* [zig](https://ziglang.org/): a low level language
* [Docker](https://www.docker.com/), [docker compose](https://docs.docker.com/compose/)
  * [Alpine](https://hub.docker.com/_/alpine)
    * [Add testing repo to alpine for install zig by repo](https://stackoverflow.com/questions/52899227/alpine-add-package-from-edge-repository)
* [python](https://www.python.org/)
  * [jupyter](https://jupyter.org/)
    * [Making Simple python wrapper kernels](https://jupyter-client.readthedocs.io/en/latest/wrapperkernels.html): basic explanation and [Example here](https://gitlab.com/nicolalandro/study-jupyter-kernel-creation)
