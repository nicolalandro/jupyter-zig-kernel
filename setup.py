import setuptools

import os
with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'jupyter_zig_kernel/pypi_readme.md'), 'r') as f:
  long_des = f.read()

setuptools.setup(
    name='jupyter_zig_kernel',
    version='0.0.1',
    description='Jupyter kernel for zig language',
    author='Nicola Landro',
    author_email='nicolaxx94@live.it',
    license='MIT',
    classifiers=[
        'License :: OSI Approved :: MIT License',
    ],
    url='https://gitlab.com/nicolalandro/jupyter-zig-kernel',
    project_urls={
        'Source': 'https://gitlab.com/nicolalandro/jupyter-zig-kernel',
    },
    packages=setuptools.find_packages(),
    scripts=['jupyter_zig_kernel/install_zig_kernel'],
    keywords=['jupyter', 'notebook', 'kernel', 'zig'],
    include_package_data=True
)
