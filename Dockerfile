FROM python:3.10-alpine

# Install ZIG
# RUN apk add libgdiplus --repository=http://dl-cdn.alpinelinux.org/alpine/edge/testing/
RUN echo "@testing https://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN apk update && apk add zig@testing

# Install Jupyter
RUN apk add build-base python3-dev libffi-dev linux-headers musl-dev gcc
RUN pip install --upgrade pip && pip install jupyter

# Install Zig Kernel
WORKDIR /code
COPY .  .
RUN python setup.py install
RUN install_zig_kernel

# Run jupyter
CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''